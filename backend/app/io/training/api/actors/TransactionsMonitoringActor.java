package io.training.api.actors;

import akka.actor.AbstractActorWithTimers;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import com.mongodb.client.ChangeStreamIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.model.changestream.ChangeStreamDocument;
import io.training.api.models.Transaction;
import io.training.api.mongo.IMongoDB;
import play.libs.Json;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;

public class TransactionsMonitoringActor extends AbstractActorWithTimers {
    protected final String SCHEDULE_KEY = "MONGO_WATCHER";

    private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);

    private static final String PING = "PING";
    private static final String PONG = "PONG";

    private IMongoDB mongoDB;

    private Executor executor;

    private ActorRef out;

    private ChangeStreamIterable<Transaction> watcher;

    public static Props props(ActorRef out, IMongoDB mongoDB, Executor executor) {
        return Props.create(TransactionsMonitoringActor.class, () -> new TransactionsMonitoringActor(out, mongoDB, executor));
    }

    private TransactionsMonitoringActor(ActorRef out, IMongoDB mongoDB, Executor executor) {
        this.mongoDB = mongoDB;
        this.out = out;
        this.executor = executor;
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(String.class, this::onMessageReceived)
                .match(MongoMonitoringActorProtocol.WATCH.class, this::onHandleWatchSignal)
                .build();
    }

    @Override
    public void preStart() {
        this.watch();
    }

    @Override
    public void postStop() {
        this.close();
    }

    private void onHandleWatchSignal(MongoMonitoringActorProtocol.WATCH ignore) {
        this.watch();
    }

    private void watch() {
        this.close();
        log.info("Watch!");
        out.tell("Starting to Watch", getSelf());
        CompletableFuture.runAsync(() -> {
            getTimers().cancel(SCHEDULE_KEY);
            try {
                MongoCollection<Transaction> userMongoCollection = mongoDB.getMongoDatabase().getCollection("transactions", Transaction.class);

                // Watch everything that happens
                watcher = userMongoCollection.watch();
                out.tell("Registering", getSelf());
                MongoCursor<ChangeStreamDocument<Transaction>> cursor = watcher.iterator();

                while (!getSelf().isTerminated()) {
                    ChangeStreamDocument<Transaction> item = cursor.tryNext();
                    if (item == null) {
                        continue;
                    }
                    log.info("Got message back {}", item.toString());
                    out.tell(Json.toJson(item.getFullDocument()).toString(), getSelf());
                }
                log.info("Closing Cursor");
                cursor.close();
            } catch (Exception ex) {
                ex.printStackTrace();
                // Couldn't do it, try again
                if (getSelf().isTerminated()) {
                    return;
                }
                getTimers().startSingleTimer(SCHEDULE_KEY, new MongoMonitoringActorProtocol.WATCH(), Duration.of(5, ChronoUnit.SECONDS));
                out.tell("Failed to register, trying again!", getSelf());
            }
        }, executor);

    }

    /**
     * Receiver of socket messages coming from the front end
     *
     * @param message
     */
    public void onMessageReceived(String message) {
        if (message.equals(PING)) {
            out.tell(PONG, getSelf());
        }
    }

    private void close() {
        if (watcher == null) {
            return;
        }
        try {
            watcher.iterator().close();
            watcher.cursor().close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}


