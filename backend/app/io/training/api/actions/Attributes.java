package io.training.api.actions;

import io.training.api.models.Taxi;
import play.libs.typedmap.TypedKey;

/**
 * Created by Agon on 09/08/2020
 */
public class Attributes {
	public static final TypedKey<Taxi> TAXI_TYPED_KEY = TypedKey.<Taxi>create("taxi");
}
