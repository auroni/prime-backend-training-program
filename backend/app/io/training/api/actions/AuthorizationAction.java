package io.training.api.actions;

import play.mvc.Action;
import play.mvc.Http;
import play.mvc.Result;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

public class AuthorizationAction extends Action<Authorized> {
    @Override
    public CompletionStage<Result> call(Http.Request request){
            if (!(request.hasHeader("Authorization") &&
                    request.getHeaders().get("Authorization").get().equals("Admin"))) {
                return CompletableFuture.supplyAsync(() -> status(Http.Status.FORBIDDEN, "forbidden"));
            }
            return delegate.call(request);
    }
}
