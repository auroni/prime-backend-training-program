package io.training.api.controllers;

import com.google.common.base.Strings;
import com.google.inject.Inject;
import com.mongodb.client.model.Filters;
import io.training.api.models.User;
import io.training.api.mongo.IMongoDB;
import org.bson.types.ObjectId;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class LectureElevenController extends Controller {
	@Inject
	IMongoDB mongoDB;

	/**
	 * The add method, returns the sum of parameter a with b
	 *
	 * @param a the first double
	 * @param b the second double
	 * @return a + b the sum of both
	 */
	public double add(double a, double b) {
		return a + b;
	}

	/**
	 * Fetches a user by id
	 *
	 * @param id user Id
	 * @return the requested user
	 * @throws NoSuchElementException   if the user was not found
	 * @throws IllegalArgumentException if the id is not written correctly
	 * @see User
	 */
	public User getById(String id) throws NoSuchElementException, IllegalArgumentException {
		ObjectId identifier = new ObjectId(id);
		User found = mongoDB
				.getMongoDatabase()
				.getCollection("users", User.class)
				.find(Filters.eq("_id", identifier))
				.first();
		if (found == null) {
			throw new NoSuchElementException("User not found");
		}
		return found;
	}

	public Result count() {
		double result = this.add(2, 5);
		return ok(Json.toJson(Json.newObject()));
	}

	//	@ApiResponses(Array(
//			new ApiResponse(code = 400, message = "Invalid ID supplied"),
//			new ApiResponse(code = 404, message = "Pet not found")))
	//@ApiParam(value = "ID of the pet to fetch")
	public Result all(String id) {
		List<User> items = mongoDB
				.getMongoDatabase()
				.getCollection("users", User.class)
				.find()
				.into(new ArrayList<>());

		return ok(Json.toJson(items));
	}
}