package io.training.api.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import io.training.api.exceptions.RequestException;
import io.training.api.models.SimpleUser;
import io.training.api.models.validators.HibernateValidator;
import io.training.api.services.SerializationService;
import io.training.api.utils.DatabaseUtils;
import org.bson.types.ObjectId;
import play.cache.AsyncCacheApi;
import play.cache.NamedCache;
import play.libs.Json;
import play.mvc.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.concurrent.CompletionStage;

public class AssignmentLectureSixController extends Controller {

    @Inject
    SerializationService serializationService;

    @Inject
    @NamedCache("redis")
    AsyncCacheApi cacheApi;

    @BodyParser.Of(BodyParser.Json.class)
    public CompletionStage<Result> setup(Http.Request request) {
        return serializationService
                .parseListBodyOfType(request, SimpleUser.class)
                .thenCompose(usersData -> {
                    // Validate data
                    HibernateValidator.validate(usersData);
                    // Save data in cache
                    cacheApi.set("usersTest", Json.toJson(usersData).toString());
                    return serializationService.toJsonNode(usersData);
                })
                .thenApply(Results::ok);
    }

    public CompletionStage<Result> all(Http.Request request) {
        return cacheApi.getOptional("usersTest")
                .thenCompose(data -> serializationService.toJsonNode(data)).thenApply(Results::ok);
    }

    public CompletionStage<Result> save(Http.Request request) {

        return serializationService
                .parseBodyOfType(request, SimpleUser.class)
                .thenCompose(user -> {
                    user.setId(UUID.randomUUID().toString());
                    // Validate data
                    this.throwableValidation(user);
                    // Save data in cache
                    return cacheApi.getOptional("usersTest")
                            .thenCompose(users -> this.parseJsonStringToUsersArray(users.get().toString()))
                            .thenCompose(users -> {
                                users.add(user);
                                cacheApi.set("usersTest", Json.toJson(users).toString());
                                return serializationService.toJsonNode(user);
                            });
                })
                .thenApply(Results::ok)
                .exceptionally(DatabaseUtils::throwableToResult);
    }

    public CompletionStage<Result> update(Http.Request request, String id) {
        return serializationService
                .parseBodyOfType(request, SimpleUser.class)
                .thenCompose(user -> {
                    user.setId(id);
                    // Validate data
                    HibernateValidator.validate(user);
                    // Update data in cache if exists, if not throw 404
                    return cacheApi.getOptional("usersTest")
                            .thenCompose(users ->  this.parseJsonStringToUsersArray(users.get().toString()))
                            .thenCompose(users -> {
                                try{
                                    List<SimpleUser> updatedList = new ArrayList();
                                    boolean exists = false;
                                    for (SimpleUser u : users) {
                                        if (u.getId().equals(id)) {
                                            updatedList.add(user);
                                            exists = true;
                                        } else {
                                            updatedList.add(u);
                                        }
                                    }

                                    if (!exists){
                                        throw new RequestException(Http.Status.NOT_FOUND, "not_found");
                                    }
                                    cacheApi.set("usersTest", Json.toJson(updatedList).toString());
                                    return serializationService.toJsonNode(user);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    throw  new CompletionException(e);
                                }
                            });
                })
                .thenApply(Results::ok)
                .exceptionally(DatabaseUtils::throwableToResult);
    }

    public CompletableFuture<Result> delete(Http.Request request, String id){
        return serializationService
                .parseBodyOfType(request, SimpleUser.class)
                .thenCompose(user -> {
                    user.setId(id);
                    // Update data in cache if exists, if not throw 404
                    return cacheApi.getOptional("usersTest")
                            .thenCompose(users ->  this.parseJsonStringToUsersArray(users.get().toString()))
                            .thenCompose(users -> {
                                try{
                                    List<SimpleUser> updatedList = new ArrayList();
                                    boolean exists = false;
                                    for (SimpleUser u : users) {
                                        if (u.getId().equals(id)) {
                                            updatedList.remove(user);
                                            exists = true;
                                        } else {
                                            updatedList.add(u);
                                        }
                                    }

                                    if (!exists){
                                        throw new RequestException(Http.Status.NOT_FOUND, "not_found");
                                    }
                                    cacheApi.set("usersTest", Json.toJson(updatedList).toString());
                                    return serializationService.toJsonNode(user);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    throw  new CompletionException(e);
                                }
                            });
                })
                .thenApply(Results::ok)
                .exceptionally(DatabaseUtils::throwableToResult);
    }

    private CompletableFuture<List<SimpleUser>> parseJsonStringToUsersArray(String jsonString) {
        ObjectMapper mapper = new ObjectMapper();

        return CompletableFuture.supplyAsync(() -> {
            try {
                return mapper.readValue(jsonString, new TypeReference<List<SimpleUser>>() {});
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        });
    }

    private <T> String throwableValidation(T data){
        try {
            // Validate data
            String validationMessage = HibernateValidator.validate(data);
            if (!validationMessage.equals("")) {
                throw new RequestException(Http.Status.BAD_REQUEST, "validation_failure");
            }
            return validationMessage;
        }
        catch (Exception ex){
            throw new CompletionException(ex);
        }
    }




}