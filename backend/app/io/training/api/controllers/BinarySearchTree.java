package io.training.api.controllers;

import io.training.api.models.BinaryTree;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class BinarySearchTree {

    BinaryTree root;
    BinarySearchTree() {
        root = null;
    }

    // This method mainly calls insertRecursive()
    void insert(Integer value) {
        root = insertRecursive(root, value);
    }

    // A recursive function to insert a new value in BST
    BinaryTree insertRecursive(BinaryTree root, Integer value) {

        if (root == null) {
            root = new BinaryTree();
            root.setValue(value);
            return root;
        }

        if (value < root.getValue())
            root.setLeft(insertRecursive(root.getLeft(), value));
        else if (value > root.getValue())
            root.setRight(insertRecursive(root.getRight(), value));

        return root;
    }

    // This method search for a Node in binary tree.
    public static BinaryTree search(BinaryTree root, int value) {
        if (root == null || root.getValue() == value)
            return root;

        if (root.getValue() > value)
            return search(root.getLeft(), value);

        return search(root.getRight(), value);
    }
}
