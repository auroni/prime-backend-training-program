package io.training.api.controllers;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.stream.Materializer;
import com.fasterxml.jackson.databind.JsonNode;
import io.training.api.actions.Authorized;
import io.training.api.actors.ConfiguredActorProtocol;
import io.training.api.actors.HelloActor;
import io.training.api.actors.HelloActorProtocol;
import io.training.api.actors.MyWebSocketActor;
import io.training.api.executors.SingleThreadedExecutionContext;
import io.training.api.models.*;
import io.training.api.services.BacktrackPathsService;
import io.training.api.services.SerializationService;
import play.cache.NamedCache;
import play.cache.SyncCacheApi;
import play.libs.streams.ActorFlow;
import play.mvc.*;
import scala.compat.java8.FutureConverters;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.stream.Collectors;

import static akka.pattern.Patterns.ask;

@Singleton
public class LectureSevenController extends Controller {
    private final ActorRef helloActor;
    private ActorRef configuredActor;
    private final ActorSystem actorSystem;
    private final Materializer materializer;

    //private static HashMap<String,User> users = new HashMap<>();
    @Inject
    SerializationService serializationService;

    @Inject
    SingleThreadedExecutionContext executionContext;

    @Inject
    @NamedCache("redis")
    SyncCacheApi cacheApi;

    @Inject
    public LectureSevenController(ActorSystem system, Materializer materializer, @Named("configured-actor") ActorRef configuredActor) {
        this.helloActor = system.actorOf(HelloActor.getProps());
        this.configuredActor = configuredActor;
        this.actorSystem = system;
        this.materializer = materializer;
    }

    public CompletionStage<Result> sayHello(String name) {
        return FutureConverters.toJava(ask(helloActor, new HelloActorProtocol.SayHello(name), 1000))
                .thenApply(response -> ok((JsonNode) response));
    }

    public CompletionStage<Result> getConfig() {
        return FutureConverters.toJava(ask(configuredActor, new ConfiguredActorProtocol.GetConfig(), 1000))
                .thenApply(response -> ok((String) response));
    }

    public WebSocket socket() {
        return WebSocket.Text.accept(
                request -> ActorFlow.actorRef(MyWebSocketActor::props, actorSystem, materializer));
    }

    /**
     * Finds the top 4 richest users above 40 years old, based on their total assets!
     */
    @BodyParser.Of(BodyParser.Json.class)
    public CompletableFuture<Result> findTopFourRichest(Http.Request request) {

        return serializationService.parseListBodyOfType(request, UserWithAssets.class)
                .thenCompose((users) -> {
                    List<UserWithAssets> result = users
                            .stream()
                            .filter(user -> user.getAge() > 40) /* Filer users above 40 years */
                            .sorted(Comparator.comparingLong(o -> sumOfAssets(o.getAssets()))) /* Sorting users by sum of assets */
                            .collect(Collectors.toList());

                    Collections.reverse(result);

                    return serializationService.toJsonNode(result.subList(0, 4));
                })
                .thenApply(Results::ok);
    }

    /**
     * Sort the users by their first name and last name initials (Agon Lohaj -> AL).
     * If initials repeat, then sort on Age. Both initials and Age are sorted descending
     */
    @BodyParser.Of(BodyParser.Json.class)
    public CompletableFuture<Result> sortByInitialsOrAge(Http.Request request) {
        return serializationService.parseListBodyOfType(request, UserWithAssets.class)
                .thenCompose((users) -> {
                    List<UserWithAssets> result = users
                            .stream()
                            .sorted(Comparator.comparing((UserWithAssets o) -> findInitials(o.getName(), o.getLastName())) // Sorting by initials
                                    .thenComparing(UserWithAssets::getAge).reversed()) // than sorting by age
                            .collect(Collectors.toList());

                    return serializationService.toJsonNode(result);
                })
                .thenApply(Results::ok);
    }

    /**
     * For the given rectangles and a point check if the point is contained within one of the rectangles.
     * If so, return the rectangle otherwise return the closest rectangle to the point!
     * A rectangle is said to be the closest if the distance between its center and the point is lowest!
     */
    @BodyParser.Of(BodyParser.Json.class)
    public CompletableFuture<Result> closestRectangle(Http.Request request) {
        return serializationService.parseBodyOfType(request, PointRectangles.class)
                .thenCompose((input) -> {
                    Point point = input.getPoint();
                    List<Rectangle> result = input.getRectangles().stream().filter(rectangle ->
                            rectangle.getX() <= point.getX() && rectangle.getY() <= point.getY() &&
                                    rectangle.getX() + rectangle.getWidth() >= point.getX() && rectangle.getY() + rectangle.getHeight() >= point.getY()
                    ).collect(Collectors.toList());

                    if (result.isEmpty()) {
                        // If empty then find closest
                        result = this.findClosestRec(input);
                    }
                    return serializationService.toJsonNode(result.get(0));
                })
                .thenApply(Results::ok);

    }

    /**
     * Return a new list, for which the companies are nested based on their hierarchy! Don't return the id if its null!
     */
    @BodyParser.Of(BodyParser.Json.class)
    public CompletableFuture<Result> hierarchy(Http.Request request) {
        return serializationService.parseListBodyOfType(request, Company.class)
                .thenCompose((companies) -> {

                    List<Company> result = buildHierarchy(companies);

                    return serializationService.toJsonNode(result);
                })
                .thenApply(Results::ok);
    }

    /**
     * Only allow the following restricted method to be called if at the headers you see a key:
     * "Authorization" and value: "Admin"
     */
    @Authorized
    public CompletableFuture<Result> restricted(Http.Request request) {
        Map<String, String> result = new HashMap();
        result.put("message", "Ok!");

        return serializationService.toJsonNode(result)
                .thenApply(Results::ok);
    }

    /**
     * Cache the value if it doesn't exist within cache for 1 second.
     * If it exists return it. Also make sure that the requests are ran synchronously, since we don't want a race condition
     */
    @BodyParser.Of(BodyParser.Json.class)
    public CompletionStage<Result> cached(Http.Request request) {
        return serializationService
                .parseBodyOfType(request, String.class)
                .thenCompose(value -> CompletableFuture.supplyAsync(() -> cacheApi.getOrElseUpdate("cache", () -> value, 1), executionContext))
                .thenCompose((result) -> serializationService.toJsonNode(result))
                .thenApply(Results::ok);
    }

    /**
     * Using Backtracking technique I will find all the paths from a source to a destination
     */


    @Inject
    BacktrackPathsService backtrackPathsService;

    @BodyParser.Of(BodyParser.Json.class)
    public CompletableFuture<Result> backtracking(Http.Request request) {
        return serializationService.parseBodyOfType(request, BacktrackModel.class)
                .thenCompose((value) -> serializationService.toJsonNode(backtrackPathsService.findPath(value)))
                .thenApply(Results::ok);
    }


    private List<Company> buildHierarchy(List<Company> companies) {
        for (Company current : companies) {
            List<Company> childCompanies = companies
                    .stream()
                    .filter(com -> com.getParentId() != null && com.getParentId().equals(current.getId()))
                    .collect(Collectors.toList());
            if (childCompanies.size() > 0) {
                current.setChildren(childCompanies);
                buildHierarchy(childCompanies);
            }
        }

        return companies.stream().filter(com -> com.getParentId() == null).collect(Collectors.toList());
    }


    private List<Rectangle> findClosestRec(PointRectangles input) {
        List<Rectangle> foundRectangle = input.getRectangles()
                .stream()
                .reduce(new ArrayList<HashMap<String, Object>>(), (total, rectangle) -> {

                    double centerX = rectangle.getX() + (rectangle.getWidth() / 2);
                    double centerY = rectangle.getY() + (rectangle.getHeight() / 2);
                    HashMap<String, Object> recDistance = new HashMap();
                    double distance = Math.sqrt(Math.pow(centerX - input.getPoint().getX(), 2) + Math.pow(centerY - input.getPoint().getY(), 2));
                    recDistance.put("distance", distance);
                    recDistance.put("rectangle", rectangle);
                    total.add(recDistance);
                    return total;
                }, (a, b) -> {
                    a.addAll(b);
                    return a;
                })
                .stream()
                .sorted(Comparator.comparingDouble(u -> (double) u.get("distance")))
                .map(item -> (Rectangle) item.get("rectangle"))
                .limit(1).collect(Collectors.toList());

        return foundRectangle;
    }


    // Return initials of name and lastname
    private String findInitials(String name, String lastname) {
        return name.substring(0, 1) + lastname.substring(0, 1);
    }

    // Find total sum of list of assets
    private long sumOfAssets(List<Asset> assets) {
        return assets.stream().mapToLong(Asset::getValue).sum();
    }
}