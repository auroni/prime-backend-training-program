package io.training.api.controllers;

import com.fasterxml.jackson.databind.JsonNode;
import io.training.api.models.BinaryTree;
import io.training.api.models.User;
import io.training.api.services.SerializationService;
import io.training.api.utils.DatabaseUtils;
import com.google.inject.Inject;
import io.training.api.models.BinaryTree;
import io.training.api.models.User;
import io.training.api.models.requests.BinaryTreeRequest;
import io.training.api.models.requests.NameValuePair;
import io.training.api.models.responses.ChartData;
import io.training.api.services.AssignemntTwoService;
import io.training.api.services.SerializationService;
import io.training.api.utils.DatabaseUtils;
import jnr.ffi.annotations.In;
import org.bson.types.ObjectId;
import play.libs.Json;
import play.mvc.*;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

public class AssignmentLectureThreeController extends Controller {
	private static List<User> users = new ArrayList<>();

	@Inject
	SerializationService serializationService;

	public CompletableFuture<Result> all(Http.Request request) {
		// Get all users
		return serializationService
				.toJsonNode(users).thenApply(Results::ok);
	}

	@BodyParser.Of(BodyParser.Json.class)
	public CompletableFuture<Result> save(Http.Request request) {
		//  Add a new user
		return serializationService.parseBodyOfType(request, User.class)
				.thenCompose((user) -> {
					user.setId(new ObjectId());
					users.add(user);
					return serializationService.toJsonNode(user);
				})
				.thenApply(Results::ok);
	}

	// Update user
	@BodyParser.Of(BodyParser.Json.class)
	public CompletableFuture<Result> update(Http.Request request, String id) {
		return serializationService.parseBodyOfType(request, User.class)
				.thenCompose((user) -> {
					users.stream().map(current -> {
						if(current.getId().equals(new ObjectId(id))){
							return user;
						}
						return current;
					}).collect(Collectors.toList());
					return serializationService.toJsonNode(user);
				})
				.thenApply(Results::ok);
	}

	// Delete user
	@BodyParser.Of(BodyParser.Json.class)
	public CompletableFuture<Result> delete(Http.Request request, String id) {
		return serializationService.parseBodyOfType(request, User.class)
				.thenCompose((user) -> {
					users.removeIf(current -> current.getId().equals(new ObjectId(id)));
					return serializationService.toJsonNode(user);
				})
				.thenApply(Results::ok);
	}

	public CompletableFuture<Result> averages(Http.Request request) {
		// Listing all unique types and summing age and count them.
		HashMap<String, HashMap<String,Double>> allUniqueTypes = users.stream().reduce(
				new HashMap<>(),
				(accumulator, current) -> {
					if(accumulator.containsKey(current.getType())){
						Double totalSum =  accumulator.get(current.getType()).get("sum") + current.getAge();
						Double count =  accumulator.get(current.getType()).get("count") + 1;
						HashMap<String, Double> result = new HashMap<>();
						result.put("sum", totalSum);
						result.put("count", count);
						accumulator.put(current.getType(), result);
					} else {
						HashMap<String, Double> result = new HashMap<>();
						result.put("sum", (double) current.getAge());
						result.put("count", 1d);
						accumulator.put(current.getType(), result);
					}
					return accumulator;
				}, (a, b) -> {
					// combiner
					a.putAll(b);
					return a;
				});

		// Calculate the averages age between of allUniques types
		List<ChartData> avgs = allUniqueTypes.entrySet().stream().map(elemt -> {
			Double avg = elemt.getValue().get("sum") / elemt.getValue().get("count");
			return new ChartData(elemt.getKey(), avg);
		}).collect(Collectors.toList());

		return serializationService
				.toJsonNode(avgs).thenApply(Results::ok);

	}

	public CompletableFuture<Result> types(Http.Request request) {
		// List and count males and females.
		HashMap<String, Integer> allUnqiueTypes = users.stream().reduce(
				new HashMap<>(),
				(accumulator, current) -> {
					if(accumulator.containsKey(current.getGender())){
						accumulator.put(current.getGender(), 1 + accumulator.get(current.getGender()));
					} else {
						accumulator.put(current.getGender(), 1);
					}
					return accumulator;
				}, (a, b) -> {
					// combiner
					a.putAll(b);
					return a;
				});
		// Returning the number of male vs female
		List<ChartData> maleVersusFemaleData = allUnqiueTypes
				.entrySet().stream()
				.map(elemt -> new ChartData(elemt.getKey(), elemt.getValue())).collect(Collectors.toList());

		return serializationService
				.toJsonNode(maleVersusFemaleData).thenApply(Results::ok);
	}

	private BinaryTree binaryTreeSearch(BinaryTree tree, int value) {
		// TODO: Implement this method
		return new BinaryTree();
	}

	@BodyParser.Of(BodyParser.Json.class)
	public CompletableFuture<Result> binaryTree(Http.Request request) {
		// Create Binary Tree from list
		BinarySearchTree binaryTree = new BinarySearchTree();
		List<Integer> treeList = Arrays.asList(27,14,10,19,35,31,42);
		treeList.forEach(binaryTree::insert);

		return serializationService
			.parseBodyOfType(request, BinaryTree.class)
			.thenCompose((data) -> {
				BinaryTree result = BinarySearchTree.search(binaryTree.getRoot(),data.getValue());
				result = result != null ? result : new BinaryTree();
				return serializationService.toJsonNode(result);
			})
			.thenApply(Results::ok);
	}
}