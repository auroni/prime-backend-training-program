package io.training.api.controllers;
import akka.stream.IOResult;
import akka.stream.javadsl.FileIO;
import akka.stream.javadsl.Source;
import akka.util.ByteString;
import com.google.inject.Inject;
import play.http.HttpEntity;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.Controller;
import play.mvc.ResponseHeader;
import play.mvc.Result;
import play.mvc.Results;

import java.util.Collections;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

public class ApplicationController extends Controller {
	private final HttpExecutionContext ec;

	@Inject
	public ApplicationController (HttpExecutionContext ec) {
		this.ec = ec;
	}

	public CompletableFuture<Result> healthCheck() {
		return CompletableFuture.supplyAsync(Results::ok, ec.current());
	}

	public Result show(String page) {
		java.io.File file = new java.io.File("/tmp/fileToServe.pdf");
		java.nio.file.Path path = file.toPath();
		Source<ByteString, CompletionStage<IOResult>> source = FileIO.fromPath(path);
		return new Result(
			new ResponseHeader(200, Collections.emptyMap()),
			new HttpEntity.Streamed(source, Optional.empty(), Optional.of("text/plain"))
		);
	}
}