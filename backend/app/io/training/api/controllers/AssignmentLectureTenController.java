package io.training.api.controllers;

import akka.actor.ActorSystem;
import akka.stream.Materializer;
import com.google.inject.Inject;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Accumulators;
import com.mongodb.client.model.Aggregates;
import com.mongodb.client.model.Projections;
import com.mongodb.client.model.Sorts;
import io.training.api.actors.TransactionsMonitoringActor;
import io.training.api.exceptions.RequestException;
import io.training.api.models.Transaction;
import io.training.api.mongo.IMongoDB;
import io.training.api.services.SerializationService;
import io.training.api.utils.DatabaseUtils;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import play.Logger;
import play.libs.concurrent.HttpExecutionContext;
import play.libs.streams.ActorFlow;
import play.mvc.*;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.stream.Collectors;

public class AssignmentLectureTenController extends Controller {
    @Inject
    IMongoDB mongoDB;

    @Inject
    SerializationService serializationService;

    public CompletableFuture<Result> assignment1 (Http.Request request) {

        MongoCollection<Transaction> collection = mongoDB
                .getMongoDatabase()
                .getCollection("transactions", Transaction.class);

        List<Bson> pipeline = new ArrayList<>();

        // Select stage
        pipeline.add(Aggregates.project(
                    Projections.fields(
                        Projections.include("categoryName"),
                        Projections.include("salesIncVatActual"))));

        // Group By stage
        pipeline.add(Aggregates.group(new Document("name", "$categoryName"),
                        Accumulators.sum("sales", "$salesIncVatActual")));

        // Project stage
        pipeline.add(Aggregates.project(
                Projections.fields(
                        Projections.computed("name", "$_id.name"),
                        Projections.exclude("_id"),
                        Projections.computed("value", "$sales"))));

        // Sort by category name - 1 for ascending
        pipeline.add(Aggregates.sort(new Document("name", 1)));

        Logger.of(this.getClass()).debug("Executing Pipeline with:");
        pipeline.forEach((next) -> Logger.of(this.getClass()).debug("Stage {}", next.toString()));

        List<Document> queryResult = collection
                .aggregate(pipeline, Document.class)
                .into(new ArrayList<>());

        return serializationService.toJsonNode(this.addChartConfigurations(queryResult))
                .thenApply(Results::ok)
                .exceptionally(DatabaseUtils::throwableToResult);
    }

    public CompletableFuture<Result> assignment2 (Http.Request request) {

        MongoCollection<Transaction> collection = mongoDB
                .getMongoDatabase()
                .getCollection("transactions", Transaction.class);

        List<Bson> pipeline = new ArrayList<>();

        // Group By stage
        pipeline.add(Aggregates.group(new Document("brandName", "$brandName")
                        .append("categoryName", "$categoryName"),
                Accumulators.sum("sales", "$salesIncVatActual")));

        // Project stage
        pipeline.add(Aggregates.project(
                Projections.fields(
                        Projections.computed("categoryName", "$_id.categoryName"),
                        Projections.computed("name", "$_id.brandName"),
                        Projections.exclude("_id"),
                        Projections.computed("value", "$sales"))));

        // Sort by category name - 1 for ascending
        pipeline.add(Aggregates.sort(new Document("name", 1)));

        // Another group by stage to build structure by brand
        pipeline.add(Aggregates.group(new Document("categoryName", "$categoryName"),
                Accumulators.push("data", "$$ROOT")));

        pipeline.add(Aggregates.project(
                Projections.fields(
                        Projections.computed("name", "$_id.categoryName"),
                        Projections.computed("data", "$data"),
                        Projections.computed("type", "bar"),
                        Projections.exclude("_id"))));

        Logger.of(this.getClass()).debug("Executing Pipeline with:");
        pipeline.forEach((next) -> Logger.of(this.getClass()).debug("Stage {}", next.toString()));

        List<String> uniques = collection
                .distinct("brandName", String.class)
                .into(new ArrayList<>()).stream().sorted().collect(Collectors.toList());
        System.out.println(uniques);

        List<Document> queryResult = collection
                .aggregate(pipeline, Document.class)
                .into(new ArrayList<>());

        Document response = new Document("xAxis", new Document("data", uniques).append("type", "category"))
                .append("series", queryResult)
                .append("yAxis", new Document("type", "value"));
        return serializationService.toJsonNode(response)
                .thenApply(Results::ok)
                .exceptionally(DatabaseUtils::throwableToResult);
    }

    public CompletableFuture<Result> assignment3 (Http.Request request) {

        MongoCollection<Transaction> collection = mongoDB
                .getMongoDatabase()
                .getCollection("transactions", Transaction.class);

        List<Bson> pipeline = new ArrayList<>();
        // Group By stage
        pipeline.add(Aggregates.group(new Document("name", "$categoryName"),
                Accumulators.avg("avgSales", "$salesIncVatActual")));

        // Project stage
        pipeline.add(Aggregates.project(
                Projections.fields(
                        Projections.computed("name", "$_id.name"),
                        Projections.exclude("_id"),
                        Projections.computed("value", "$avgSales"))));

        // Sort by avgSales
        pipeline.add(Aggregates.sort(new Document("avgSales", -1)));
        pipeline.add(Aggregates.limit(10));

        Logger.of(this.getClass()).debug("Executing Pipeline with:");
        pipeline.forEach((next) -> Logger.of(this.getClass()).debug("Stage {}", next.toString()));

        List<Document> queryResult = collection
                .aggregate(pipeline, Document.class)
                .into(new ArrayList<>());

        Map<String, Object> result = new HashMap<>();
        Map<String, Object> series = new HashMap<>();
        series.put("data", queryResult);
        series.put("type", "treemap");
        result.put("series", series);

        return serializationService.toJsonNode(result)
                .thenApply(Results::ok)
                .exceptionally(DatabaseUtils::throwableToResult);
    }

    public CompletableFuture<Result> assignment4 (Http.Request request) {

        MongoCollection<Transaction> collection = mongoDB
                .getMongoDatabase()
                .getCollection("transactions", Transaction.class);

        List<Bson> pipeline = new ArrayList<>();

        // Group By stage
        pipeline.add(Aggregates.group(new Document("name", "$categoryName"),
                Accumulators.sum("value", "$volume"),
                Accumulators.sum("sales", "$salesIncVatActual")
                ));

        // Sort by highest sales
        pipeline.add(Aggregates.sort(new Document("sales", -1)));
        pipeline.add(Aggregates.limit(4));

        // Project stage
        pipeline.add(Aggregates.project(
                Projections.fields(
                        Projections.computed("name", "$_id.name"),
                        Projections.exclude("_id"),
                        Projections.computed("value", "$value"))));

        Logger.of(this.getClass()).debug("Executing Pipeline with:");
        pipeline.forEach((next) -> Logger.of(this.getClass()).debug("Stage {}", next.toString()));

        List<Document> queryResult = collection
                .aggregate(pipeline, Document.class)
                .into(new ArrayList<>());

        Double maxVal = queryResult.stream()
                .map(item-> item.getDouble("value"))
                .reduce(0d, Math::max);

        queryResult = queryResult.stream().map(item -> {
            double max = (double)item.getDouble("value") / maxVal;
            double perc = Math.max(max, 0.2);
            double emphasis = Math.min(perc + 0.2, 1);

            String normalColor = String.format("rgb(60, 185, 226, %s)", perc);
            String emphasisColor = String.format("rgb(60, 185, 226, %s)", emphasis);
            // const percentage =
            //      const emphasis = Math.min(percentage + 0.2, 1)
            item.append("itemStyle", new Document("normal", new Document("color",  normalColor))
                .append("emphasis", new Document( "color", emphasisColor)));
            return item;
        }).collect(Collectors.toList());

        Map<String, Object> result = new HashMap<>();
        Map<String, Object> series = new HashMap<>();
        series.put("data", queryResult);
        series.put("type", "pie");
        result.put("series", series);

        return serializationService.toJsonNode(result)
                .thenApply(Results::ok)
                .exceptionally(DatabaseUtils::throwableToResult);
    }

    public CompletableFuture<Result> assignment5 (Http.Request request) {

        MongoCollection<Transaction> collection = mongoDB
                .getMongoDatabase()
                .getCollection("transactions", Transaction.class);

        List<Bson> pipeline = new ArrayList<>();

        // Group By stage
        pipeline.add(Aggregates.group(new Document("name", "$brandName"),
                Accumulators.sum("value", "$volume"),
                Accumulators.sum("sales", "$salesIncVatActual")
        ));

        // Project stage
        pipeline.add(Aggregates.project(
                Projections.fields(
                        Projections.computed("name", "$_id.name"),
                        Projections.exclude("_id"),
                        Projections.include("sales"),
                        Projections.include("value"))));

        Logger.of(this.getClass()).debug("Executing Pipeline with:");
        pipeline.forEach((next) -> Logger.of(this.getClass()).debug("Stage {}", next.toString()));

        List<Document> queryResult = collection
                .aggregate(pipeline, Document.class)
                .into(new ArrayList<>())
                .stream().map(item -> {
                    Document billable = new Document();
                    billable.put("name", item.getString("name"));
                    billable.put("value", item.getDouble("sales") * item.getDouble("value"));
                    return  billable;
                }).collect(Collectors.toList());


        Map<String, Object> result = this.addChartConfigurations(queryResult);
        Map<String, Object> series = new HashMap<>();
        series.put("type", "bar");
        series.put("data", queryResult);
        result.put("series", series);
        this.addVisualMapConfig(result, queryResult);

        return serializationService.toJsonNode(result)
                .thenApply(Results::ok)
                .exceptionally(DatabaseUtils::throwableToResult);
    }

    public CompletableFuture<Result> assignment6 (Http.Request request) {

        MongoCollection<Transaction> collection = mongoDB
                .getMongoDatabase()
                .getCollection("transactions", Transaction.class);

        List<Bson> pipeline = new ArrayList<>();
        // Group By stage
        pipeline.add(Aggregates.group(new Document("name", "$brandName"),
                Accumulators.sum("value", "$salesIncVatActual")));

        // Project stage
        pipeline.add(Aggregates.project(
                Projections.fields(
                        Projections.computed("name", "$_id.name"),
                        Projections.include("value"),
                        Projections.exclude("_id"))));

        Logger.of(this.getClass()).debug("Executing Pipeline with:");
        pipeline.forEach((next) -> Logger.of(this.getClass()).debug("Stage {}", next.toString()));

        List<Document> queryResult = collection
                .aggregate(pipeline, Document.class)
                .into(new ArrayList<>());

        Document result = new Document();
        result.put("0-10", queryResult.stream().filter(item -> item.getDouble("value") <= 10).mapToDouble(item -> item.getDouble("value")).sum());
        result.put("10-100", queryResult.stream().filter(item -> item.getDouble("value") > 10 && item.getDouble("value") <= 100).mapToDouble(item -> item.getDouble("value")).sum());
        result.put("100+", queryResult.stream().filter(item -> item.getDouble("value") > 100).mapToDouble(item -> item.getDouble("value")).sum());

        List<Document> series = result.entrySet().stream()
                .map(item -> new Document("name", item.getKey()).append("value", item.getValue())).collect(Collectors.toList());

        Document response = new Document("series", new Document("type", "treemap").append("data", series));

        return serializationService.toJsonNode(response)
				.thenApply(Results::ok)
				.exceptionally(DatabaseUtils::throwableToResult);
	}
    @Inject
    private ActorSystem actorSystem;
    @Inject
    private Materializer materializer;
    @Inject
    HttpExecutionContext ec;

    public CompletableFuture<Result> paginated(Http.Request request, String token, Integer limit, Integer skip) {
		MongoCollection<Transaction> collection = mongoDB.getMongoDatabase()
				.getCollection("transactions", Transaction.class);

		if (!ObjectId.isValid(token) || limit < 1 || skip < 0) {
			throw new CompletionException(new RequestException(Http.Status.BAD_REQUEST, "bad_query_parameters"));
		}
		List<Transaction> transactions = collection
				.find()
				.limit(limit).skip(skip)
				.sort(Sorts.descending("_id")).into(new ArrayList<>());

		Logger.of(this.getClass()).debug("User started paginating at: {}", getDateFromObjectId(token));
		return serializationService.toJsonNode(transactions)
				.thenApply(Results::ok)
				.exceptionally(DatabaseUtils::throwableToResult);
	}

    private Map<String, Object> addChartConfigurations(List<Document> queryResponse) {
        Map<String, Object> result = new HashMap<>();

        // xAxis
        Map<String, Object> xAxis = new HashMap();
        xAxis.put("type", "category");
        List<String> names = queryResponse.stream().reduce(new ArrayList<String>(), (acc, current) -> {
            acc.add(current.get("name").toString());
            return acc;
        }, (a, b) -> {a.addAll(b); return a;});
        xAxis.put("data", names);

        // yAxis
        Map<String, Object> yAxis = new HashMap();
        yAxis.put("type", "value");

        // Series
        Map<String, Object> series = new HashMap();
        series.put("type", "line");
        series.put("color", "#293642");
        Map<String, String> areaStyle = new HashMap<>();
        areaStyle.put("color", "#1b9ad1");
        series.put("areaStyle", areaStyle);
        series.put("data", queryResponse);

        // Add all data and configuration
        result.put("xAxis", xAxis);
        result.put("yAxis", yAxis);
        result.put("series", series);
        return result;
    }

    private void addVisualMapConfig(Map<String, Object> response, List<Document> queryResult) {

		Double maxVal = queryResult.stream()
				.map(item -> item.getDouble("value"))
				.reduce(0d, Math::max);

		Double minVal = queryResult.stream()
				.map(item -> item.getDouble("value"))
				.reduce(0d, Math::min);

		response.put("visualMap", new Document("top", 10).append("right", 10).append("min", minVal)
				.append("max", maxVal).append("type", "continuous"));
	}
    public WebSocket subscribe() {
        return WebSocket.Text.accept(request -> ActorFlow.actorRef((out) ->
                TransactionsMonitoringActor.props(out, mongoDB, ec.current()), actorSystem, materializer));
    }

    public static Date getDateFromObjectId(String objectId) {
        long timestamp = Long.parseLong(objectId.substring(0, 8), 16) * 1000;
        return new Date(timestamp);
    }
}
