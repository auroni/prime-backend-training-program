package io.training.api.controllers;

import com.google.inject.Inject;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;
import io.training.api.exceptions.RequestException;
import io.training.api.models.User;
import io.training.api.models.validators.HibernateValidator;
import io.training.api.mongo.IMongoDB;
import io.training.api.services.SerializationService;
import org.bson.types.ObjectId;
import play.libs.Json;
import play.mvc.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

public class AssignmentLectureNineController extends Controller {


    @Inject
    IMongoDB mongoDB;

    @Inject
    SerializationService serializationService;

    @BodyParser.Of(BodyParser.Json.class)
    public CompletableFuture<Result> setup (Http.Request request) {

        return serializationService
                .parseListBodyOfType(request, User.class)
                .thenCompose((users) -> {
                    MongoCollection<User> collection = mongoDB.getMongoDatabase()
                            .getCollection("users", User.class);
                    // Insert all using insertMany operator
                    collection.insertMany(users);
                    return serializationService.toJsonNode(users);
                })
                .thenApply(Results::ok);
    }

    public CompletableFuture<Result>  all (Http.Request request) {
        MongoCollection<User> collection = mongoDB.getMongoDatabase()
                .getCollection("users", User.class);

        List<User> users = collection.find().into(new ArrayList<>());

        return serializationService.toJsonNode(users)
                .thenApply(Results::ok);
    }

    @BodyParser.Of(BodyParser.Json.class)
    public Result save (Http.Request request) {
        Optional<User> user;
        try{
            user = request.body().parseJson(User.class);
            if (!HibernateValidator.validate(user.get()).equals("")) {
                throw new RequestException(Http.Status.BAD_REQUEST, "bad_validation") ;
            }

        }catch (Exception e){
            return badRequest(Json.toJson("bad_validation"));
        }

        MongoCollection<User> collection = mongoDB.getMongoDatabase().getCollection("users", User.class);
        collection.insertOne(user.get());
        return ok(Json.toJson(user));
    }

    @BodyParser.Of(BodyParser.Json.class)
    public Result update (Http.Request request, String id) {

        if (!ObjectId.isValid(id)) {
            return notFound();
        }

        Optional<User> user;
        try{
            user = request.body().parseJson(User.class);
            if (!HibernateValidator.validate(user.get()).equals("")) {
                throw new RequestException(Http.Status.BAD_REQUEST, "bad_validation") ;
            }

        }catch (Exception e){
            return badRequest(Json.toJson("bad_validation"));
        }

        MongoCollection<User> collection = mongoDB.getMongoDatabase().getCollection("users", User.class);
        User newUser = collection.findOneAndReplace(Filters.eq("_id", new ObjectId(id)), user.get());
        if(newUser == null){
            return notFound();
        }
        return ok(Json.toJson(user));
    }

    @BodyParser.Of(BodyParser.Json.class)
    public Result delete (Http.Request request, String id) {

        if (!ObjectId.isValid(id)) {
            return notFound();
        }

        MongoCollection<User> collection = mongoDB.getMongoDatabase().getCollection("users", User.class);
        User user = collection.findOneAndDelete(Filters.eq("_id", new ObjectId(id)));
        if (user == null){
            return notFound();
        }
        return ok(Json.toJson(request.body().parseJson(User.class)));
    }
}
