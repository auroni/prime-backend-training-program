package io.training.api.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import javax.validation.constraints.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by agonlohaj on 28 Aug, 2020
 */
@ToString(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
@EqualsAndHashCode(callSuper = true)
public class UserWithAssets extends SimpleUser {
	List<Asset> assets = new ArrayList();

}
