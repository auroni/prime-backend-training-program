package io.training.api.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.training.api.mongo.serializers.ObjectIdDeSerializer;
import io.training.api.mongo.serializers.ObjectIdStringSerializer;
import lombok.*;
import org.bson.types.ObjectId;

import javax.validation.constraints.*;

/**
 * Created by agonlohaj on 28 Aug, 2020
 */
@ToString(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
@EqualsAndHashCode(of = "id")
public class SimpleUser  {
	String id;
	@NotEmpty(message = "cannot be empty")
	String type;
	@NotEmpty(message = "cannot be empty")
	@Pattern(regexp = "M|F", message = "you may feel dinosaur but i need M or F")
	String gender;
	@NotNull(message = "cannot be empty")
	@Min(value = 0, message = "Cannot be lower than 0")
	@Max(value = 99, message = "Cannot be higher than 99")
	int age;
	@NotEmpty(message = "cannot be empty")
	String name;
	@NotNull(message = "cannot be empty")
	String username;
	@NotNull(message = "canot be empty")
	String lastName;
	@NotNull(message = "canot be empty")
	String avatar;

}
