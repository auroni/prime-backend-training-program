package io.training.api.models;

import lombok.Data;

@Data
public class Asset {
    String type;
    long value;
}
