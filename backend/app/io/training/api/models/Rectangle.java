package io.training.api.models;

import lombok.Data;

@Data
public class Rectangle {
    String id;
    double x;
    double y;
    double width;
    double height;

}
