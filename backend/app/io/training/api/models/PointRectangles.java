package io.training.api.models;

import lombok.Data;

import java.util.List;

@Data
public class PointRectangles {
    Point point;
    List<Rectangle> rectangles;
}
