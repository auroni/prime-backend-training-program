package io.training.api.models;

import lombok.Data;

import java.util.List;

@Data
public class BacktrackModel {
    List<Node> nodes;
    int from;
    int to;
}
