package io.training.api.models.responses;

import lombok.Data;

/**
 * Created by agonlohaj on 02 Oct, 2020
 */
@Data
public class AggregationExample {
	Double total;
	Double sales;
	String brand;
	String category;

	public Double getAgon() {
		return total * sales;
	}
}
