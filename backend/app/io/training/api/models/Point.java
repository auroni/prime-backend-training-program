package io.training.api.models;

import lombok.Data;

@Data
public class Point {
    double x;
    double y;

}
