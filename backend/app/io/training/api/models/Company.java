package io.training.api.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class Company {
    String id;
    String name;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    String parentId;
    List<Company> children = new ArrayList<>();
}
