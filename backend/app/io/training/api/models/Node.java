package io.training.api.models;

import lombok.Data;

import java.util.List;

@Data
public class Node {
    int id;
    List<Integer> links;
}
