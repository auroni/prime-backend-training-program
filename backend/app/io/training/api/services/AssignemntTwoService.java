package io.training.api.services;

import com.google.inject.Inject;
import io.training.api.models.requests.BinarySearchRequest;
import io.training.api.models.requests.NameValuePair;
import io.training.api.models.responses.ChartData;
import jnr.ffi.annotations.In;
import play.libs.concurrent.HttpExecutionContext;

import javax.inject.Singleton;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import java.lang.Math;
import java.util.*;
/**
 * Created by Agon on 09/08/2020
 */
@Singleton
public class AssignemntTwoService {
    @Inject
    HttpExecutionContext ec;


    /**
     * Function as Line: y = x * 2
     *
     * @param input
     * @return
     */
    public CompletableFuture<List<Integer>> function1(List<Integer> input) {
        return CompletableFuture.supplyAsync(() -> {

            List<Integer> list = input.stream().map(next -> next * 2).collect(Collectors.toList());

            return new ArrayList<>(list);
        }, ec.current());
    }


    /**
     * Function as Scatter y = square root of the absolute value of ((x ^ 2) + (x * 4))
     *
     * @param input
     * @return
     */
    public CompletableFuture<List<Double>> function2(List<Integer> input) {
        return CompletableFuture.supplyAsync(() -> {
            List<Double> list = input
                    .stream()
                    .map(next -> Math.sqrt(Math.abs(Math.pow(next, 2) + (next * 4))))
                    .collect(Collectors.toList());

            return new ArrayList<>(list);
        }, ec.current());
    }

    /**
     * Function y = If 3^2 - x^2 > 0 than square root of (3^2 - x^2). If 3^2 - x^2 < 0 then - square root of absolute value of (3^2 - x^2)
     *
     * @param input
     * @return
     */

    public CompletableFuture<List<Double>> function3(List<Integer> input) {
        return CompletableFuture.supplyAsync(() -> {

            List<Double> list = input
                    .stream()
                    .map(next -> {
                        Double y = Double.valueOf(Math.pow(3,2) - Math.pow(next, 2));
                        return y > 0 ? Double.valueOf(Math.sqrt(y)) : -1 * (Math.sqrt(Math.abs(y)));
                    })
                    .collect(Collectors.toList());
            return new ArrayList<>(list);
        }, ec.current());
    }

    /**
     * Function as Line: y = sin(x)
     *
     * @param input
     * @return
     */
    public CompletableFuture<List<Double>> function4(List<Integer> input) {

        return CompletableFuture.supplyAsync(() -> {

            List<Double> list = input.stream()
                    .map(next -> Math.sin(next))
                    .collect(Collectors.toList());

            return list;
        }, ec.current());
    }

    /**
     * Function as Line: y = cos(x)
     *
     * @param input
     * @return
     */
    public CompletableFuture<List<Double>> function5(List<Integer> input) {
        return CompletableFuture.supplyAsync(() -> {

            List<Double> list = input.stream()
                    .map(next -> Math.cos(next))
                    .collect(Collectors.toList());
            return new ArrayList<>(list);
        }, ec.current());
    }

    /**
     * 2 Line Functions displayed together, one for Sin and one for Cos, check only the series option to include two of them
     * Returns two lists of double the first list for sin, and the second one for cos
     *
     * @param input
     * @return
     */
    public CompletableFuture<List<List<Double>>> function6(List<Integer> input) {
        return CompletableFuture.supplyAsync(() -> {
            List<List<Double>> result = new ArrayList<>();
            List<Double> resultSin = input.stream()
                    .map(next -> Math.sin(next))
                    .collect(Collectors.toList());
            result.add(resultSin);
            List<Double> resultCos = input.stream()
                    .map(next -> Math.cos(next))
                    .collect(Collectors.toList());
            result.add(resultCos);
            return new ArrayList<>(result);
        }, ec.current());
    }

    /**
     * I want to see the top 4 performing words, given the randomCategoryData, the top 4 with the highest random generated value
     * Make sure the values are summed on repeated words (no duplicates)
     *
     * @param input
     * @return
     */
    public CompletableFuture<List<NameValuePair>> function7(List<NameValuePair> input) {
        return CompletableFuture.supplyAsync(() -> {
            // Get the HashMap of unique names and their summed value
            HashMap<String, Integer> allUniques = input.stream().reduce(new HashMap<>(), (accumulator, current) -> {
                if(accumulator.containsKey(current.getName())){
                    Integer sum =  accumulator.get(current.getName()) + current.getValue();
                    accumulator.put(current.getName(), sum);
                } else {
                    accumulator.put(current.getName(), current.getValue());
                }
                return accumulator;
            }, (a, b) -> {
                // combiner
                a.putAll(b);
                return a;
            });

            // Sort by greatest value and get top 4 elements
            List<NameValuePair> topElements = sortMapsByValue(allUniques)
                    .stream().limit(4)
                    .map(item -> new NameValuePair(item.getKey(), item.getValue()))
                    .collect(Collectors.toList());

            return new ArrayList<>(topElements);
        }, ec.current());
    }

    private List<Map.Entry<String, Integer>> sortMapsByValue(HashMap<String, Integer> hashMap){
        // Create a list from elements of HashMap
        List<Map.Entry<String, Integer> > list = new LinkedList<Map.Entry<String, Integer> >(hashMap.entrySet());

        // Sort the list
        Collections.sort(list, new Comparator<Map.Entry<String, Integer> >() {
            public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
                return (o2.getValue()).compareTo(o1.getValue());
            }
        });

        return new ArrayList<>(list);

    }

    /**
     * Calculate the average within the groups now, and show that here. Check the random Category data on how it generates those
     *
     * @param input
     * @return
     */
    public CompletableFuture<List<ChartData>> function8(List<NameValuePair> input) {
        return CompletableFuture.supplyAsync(() -> {

            HashMap<String, HashMap<String,Double>> allUniques = input.stream().reduce(
                    new HashMap<>(),
                    (accumulator, current) -> {
                        if(accumulator.containsKey(current.getName())){
                            Double totalSum =  accumulator.get(current.getName()).get("sum") + current.getValue();
                            Double count =  accumulator.get(current.getName()).get("count") + 1;
                            HashMap<String, Double> result = new HashMap<>();
                            result.put("sum", totalSum);
                            result.put("count", count);
                            accumulator.put(current.getName(), result);
                        } else {

                            HashMap<String, Double> result = new HashMap<>();
                            result.put("sum", (double) current.getValue());
                            result.put("count", 1d);

                            accumulator.put(current.getName(), result);

                        }
                        return accumulator;
            }, (a, b) -> {
                // combiner
                a.putAll(b);
                return a;
            });

            ////Calculate the averages of allUniques
            List<ChartData> avgs = allUniques.entrySet().stream().map(elemt -> {
                Double avg = elemt.getValue().get("sum") / elemt.getValue().get("count");
                return new ChartData(elemt.getKey(), avg);
            }).collect(Collectors.toList());
//

            return new ArrayList<>(avgs);
        }, ec.current());
    }

    /**
     * Calculate the values such that they are cumulative, each subsequent is summed with the total so far!
     *
     * @param input
     * @return
     */
    public CompletableFuture<List<NameValuePair>> function9(List<NameValuePair> input) {
        return CompletableFuture.supplyAsync(() -> {

            HashMap<String, Object> cumSumsByGroup = input.stream().reduce(new HashMap<>(), (accumulator, current) -> {
                if(!accumulator.containsKey("cumulativeSum") && !accumulator.containsKey("values")){
                    int cumulative = current.getValue();
                    accumulator.put("cumulativeSum", cumulative);
                    current.setValue(cumulative);
                    List<NameValuePair> vals = new ArrayList<>();
                    vals.add(current);
                    accumulator.put("values", vals);
                }
                else {
                    int cumulative = (int)accumulator.get("cumulativeSum") + current.getValue();
                    accumulator.put("cumulativeSum", cumulative);
                    current.setValue(cumulative);
                    ArrayList<NameValuePair> vals = (ArrayList)accumulator.get("values");
                    vals.add(current);
                    accumulator.put("values", vals);
                }
                return accumulator;
            }, (a, b) -> {
                // combiner
                a.putAll(b);
                return a;
            });


            return (ArrayList<NameValuePair>)cumSumsByGroup.get("values");
        }, ec.current());
    }

    /**
     * Calculate the values such that they are cumulative, each subsequent is summed with the total so far!
     *
     * @param request
     * @return index - the index of the search
     */
    public CompletableFuture<Integer> binarySearch (BinarySearchRequest request) {
        return CompletableFuture.supplyAsync(() -> {
            Integer arraySize = request.getValues().size();
            List<Integer> array = request.getValues();
            Integer searchingNum = request.getSearch();
            Integer low = 0;

            while (low <= arraySize){
                int middle = low + (arraySize - low) / 2;
                if (array.get(middle).equals(searchingNum)){
                    return middle;
                }

                if(array.get(middle) < searchingNum)
                    low = middle + 1;

                else arraySize = middle - 1;
            }
            return -1;

        }, ec.current());
    }
}
