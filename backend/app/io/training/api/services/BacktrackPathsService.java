package io.training.api.services;

import io.training.api.models.BacktrackModel;
import io.training.api.models.Node;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class BacktrackPathsService {
    private List<List<Integer>> paths = new ArrayList<>();
    private List<Node> visited = new ArrayList<>();


    public List<List<Integer>> findPath(BacktrackModel input) {

        Optional<Node> firstNode = input.getNodes().stream().filter(node -> node.getId() == input.getFrom()).findFirst();
        if (firstNode.isPresent()){
            return new ArrayList<>();
        }

        Optional<Node> lastNode = input.getNodes().stream().filter(node -> node.getId() == input.getTo()).findFirst();
        if (lastNode.isPresent()){
            return new ArrayList<>();
        }

        visited.add(firstNode.get());

        backtrack(input.getNodes(), firstNode.get(), lastNode.get());
        Collections.reverse(paths);
        return paths;
    }

    public boolean backtrack(List<Node> nodes, Node currentNode,  Node lastNode) {
        if (currentNode.getId() == lastNode.getId()) {

            List<Integer> list = visited.stream().map(Node::getId).collect(Collectors.toList());
            paths.add(list);

            return false;
        }

        for (int i = 0; i < currentNode.getLinks().size(); i++) {

            int thisNodeIndex = currentNode.getLinks().get(i);
            Node thisNode = nodes.stream().filter(x -> x.getId() == thisNodeIndex).findFirst().get();

            if (!visited.contains(thisNode)) {
                visited.add(thisNode);

                if (backtrack(nodes, thisNode, lastNode)){
                    return true;
                }

                visited.remove(thisNode);
            }
        }

        return false;
    }
}
