/**
 * Created by LeutrimNeziri on 09/04/2019.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import React from "react";
import { PAGES } from 'Constants'
import Intro from "pages/lecture11/Intro";
import Testing from "pages/lecture11/Testing";
import Documentation from "pages/lecture11/Documentation";
import CDCI from "pages/lecture11/CDCI";
import Assignments from "pages/lecture11/Assignments";

const styles = ({ typography }) => ({
  root: {},
})

class Index extends React.Component {
  render() {
    const { classes, breadcrumbs, ...other } = this.props

    let section = breadcrumbs[0]
    if (breadcrumbs.length > 1) {
      section = breadcrumbs[1]
    }

    const props = {
      section,
      ...other
    }

    switch (section.id) {
      case PAGES.LECTURE_11.TESTING:
        return <Testing {...props} />
      case PAGES.LECTURE_11.DOCUMENTATION:
        return <Documentation {...props} />
      case PAGES.LECTURE_11.CD_CI:
        return <CDCI {...props} />
      case PAGES.LECTURE_11.ASSIGNMENTS:
        return <Assignments {...props} />
    }
    return <Intro {...props}/>
  }
}

export default withStyles(styles)(Index)
