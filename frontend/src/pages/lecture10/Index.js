/**
 * Created by Agon Lohaj on 09/04/2019.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import { PAGES } from 'Constants';
import Intro from "pages/lecture10/Intro";
import Transactions from "pages/lecture10/Transactions";
import Replication from "pages/lecture10/Replication";
import ChangeStreams from "pages/lecture10/ChangeStreams";
import Indexing from "pages/lecture10/Indexing";
import Assignments from "pages/lecture10/Assignments";
import React from "react";

const styles = () => ({
  root: {},
})

class Index extends React.Component {
  render() {
    const { breadcrumbs } = this.props

    let section = breadcrumbs[0]
    if (breadcrumbs.length > 1) {
      section = breadcrumbs[1]
    }

    const props = {
      section
    }

    switch (section.id) {
      case PAGES.LECTURE_10.TRANSACTIONS:
        return <Transactions {...props} />
      case PAGES.LECTURE_10.CHANGE_STREAMS:
        return <ChangeStreams {...props} />
      case PAGES.LECTURE_10.INDEXING:
        return <Indexing {...props} />
      case PAGES.LECTURE_10.REPLICATION:
        return <Replication {...props} />
      case PAGES.LECTURE_10.ASSIGNMENTS:
        return <Assignments {...props} />
    }
    return <Intro {...props} />
  }
}

export default withStyles(styles)(Index)
