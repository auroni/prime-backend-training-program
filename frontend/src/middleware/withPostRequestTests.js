import React from "react";
import withTests from 'middleware/withTests'

/**
 * For the given inputs, has a comparison function and a success function
 * Runs the whole tests at the backend, and then compares each of them via the comparator
 * If all are success returns a good status code
 * @param inputs
 * @param url
 * @param onCompare
 * @param onSuccess
 * @param onFailure
 * @returns {{response: any, error: , isLoading: boolean, runTests: runTests}}
 */
function withPostRequestTests (inputs, url, onCompare, onSuccess, onFailure) {
  return withTests(
    inputs,
    (input) => ({
      endpoint: url,
      options: {
        method: 'POST',
        body: JSON.stringify(input),
      }
    }),
    onCompare,
    onSuccess,
    onFailure
  )
}

export default withPostRequestTests
