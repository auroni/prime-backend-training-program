import 'whatwg-fetch'
import { API_URL } from 'Constants'

const callApi = ({ endpoint, options: optionsFromCall = {} }, store) => {
    const url = API_URL + endpoint
    let options = {
        // default params
        mode: 'cors', // no-cors, cors, *same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        redirect: 'follow', // manual, *follow, error
        referrer: 'no-referrer', // no-referrer, *client,
        headers: {
          'Content-Type': 'application/json',
          // 'Content-Type': 'application/x-www-form-urlencoded',
        },
        ...optionsFromCall
    }
    console.log(`Calling API: ${url}`, options)
    return fetch(url, options)
      .catch(() => {
        throw {
          status: 503,
          message: 'Oops. We could not load the requested data. Make sure the backend service is up and running and that the request is correct!',
        }}
      )
      .then((response) => {
        if (response.status >= 200 && response.status < 300) {
            return response
        }
        const error = {
            status: response.status,
            message: response.statusText
        }
        console.log(`Error at ${url}`, error)
        throw error
      }).then(response => {
        return response.json()
      }).then(json => {
        console.log(`Success at ${url}`, json)
        return json
      })
}


export const CALL_API = Symbol('Call API')

/**
 * Intercepts CALL_API actions to perform the call to the API server
 */
export default store => next => action => {
    const call = action[CALL_API]
    // Only apply this middleware if we are calling an API
    if (typeof call === 'undefined') {
        return next(action)
    }
    return callApi(call, store)
}
